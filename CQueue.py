from CNode import *
from CList import *
class CQueue(CList):
    def __init__(self):
        CList.__init__(self)
    def Push(self,data):
        CList.Insert(self, data, flag = 1)
    def Pop(self):
        self._CList__current = self._CList__first
        aux = self._CList__current
        CList.Remove(self)
        return aux.getData()
    def getLast(self): raise AttributeError, "Function not defined in CQueue!"
    def getFirst(self): raise AttributeError, "Function not defined in CQueue!"
    def getData(self): raise AttributeError, "Function not defined in CQueue!"
    def First(self): raise AttributeError, "Function not defined in CQueue!"
    def Last(self): raise AttributeError, "Function not defined in CQueue!"
    def Left(self): raise AttributeError, "Function not defined in CQueue!"
    def Right(self): raise AttributeError, "Function not defined in CQueue!"
    def isLast(self): raise AttributeError, "Function not defined in CQueue!"
    def isFirst(self): raise AttributeError, "Function not defined in CQueue!"
    def Insert(self, data, flag = 1): raise AttributeError, "Function not defined in CQueue!"
    def Remove(self): raise AttributeError, "Function not defined in CQueue!"
