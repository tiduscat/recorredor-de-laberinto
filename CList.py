from CNode import *
class CList(object):
    __slots__=['__size', '__first', '__last', '__current', '__iter']
    # Constructor
    def __init__(self):
        self.__size = 0
        self.__first = None
        self.__last = None
        self.__current = None
        self.__iter = None
    # Recollir dades de la llista
    def getData(self):
        if (self.__size == 0): raise IndexError
        else: return self.__current.getData()
    def getSize(self): return self.__size
    def getLast(self):
        if (self.__size == 0): raise IndexError
        else: return self.__last.getData()
    def getFirst(self):
        if (self.__size == 0): raise IndexError
        else:
            first = self.__first.getData()
            return first
    # Funcions per moure el __current
    def First(self):
        self.__current = self.__first
    def Last(self):
        self.__current = self.__last
    def Left(self):
        if (self.__current == self.__first): raise IndexError
        else: self.__current = self.__current.getLeft()
    def Right(self):
        if (self.__current == self.__last): raise IndexError
        else: self.__current = self.__current.getRight()
    # Funcions per comprovar l'estat de la llista.
    def isLast(self):
        if (self.__current == self.__last): return True
        else: return False
    def isFirst(self):
        if (self.__current == self.__first): return True
        else: return False
    def isEmpty(self):
        if (self.__size == 0): return True
        else: return False 
    # Funci� per convertir la llista en una cadena de caracters.
    def __str__(self):
        data = []
        aux = self.__first
        while (aux != None):
            data = data + [aux.getData()]
            seguent = aux.getRight()
            aux = seguent
        return str(data)  
    # Funcions d'inserci�/esborrat de dades
    def Insert(self, data, flag = 1):
        nouNode = CNode(data, None, None)
        self.__size += 1
        if (self.__size == 1):
            self.__first = nouNode
            self.__last = self.__first
            self.__current = self.__first
        else:
            if (flag == 1): #dreta del current
                if (self.__current == self.__last):
                    nouNode.setLeft(self.__current)
                    self.__current.setRight(nouNode)
                    self.__last = nouNode
                    self.__current = nouNode
                else:
                    nouNode.setRight(self.__current.getRight())
                    nouNode.setLeft(self.__current)
                    aux = self.__current.getRight()
                    aux.setLeft(nouNode)
                    self.__current.setRight(nouNode)
                    self.__current = nouNode
            if (flag == 0): #esquerra del current
                if (self.__current == self.__first):
                    nouNode.setRight(self.__current)
                    self.__current.setLeft(nouNode)
                    self.__first = nouNode
                    self.__current = self.__first
                else:
                    nouNode.setRight(self.__current)
                    nouNode.setLeft(self.__current.getLeft())
                    aux = self.__current.getLeft()
                    aux.setRight(nouNode)
                    self.__current.setLeft(nouNode)
                    self.__current = nouNode
    def Remove(self):
        if (self.__size == 0): raise IndexError
        else:
            if(self.__first == self.__last):
                self.__current = None
                self.__first = None
                self.__last = None
            else:
                if (self.__current == self.__first):
                    self.__first = self.__current.getRight()
                    self.__first.setLeft(None)
                    self.__current.setRight(None)
                    self.__current = self.__first
                elif (self.__current == self.__last):
                    self.__last = self.__current.getLeft()
                    self.__last.setRight(None)
                    self.__current.setLeft(None)
                    self.__current = self.__last
                else:
                    aux1 = self.__current
                    self.__current = aux1.getLeft()
                    self.__current.setRight(aux1.getRight())
                    aux2 = aux1.getRight()
                    aux2.setLeft(self.__current)
                    aux1.setRight(None)
                    aux1.setLeft(None)
            self.__size -= 1
    # Iteradors
    def __iter__(self):
        self.__iter = self.__first
        return self
    def next(self):
        try:
            aux = self.__iter
            self.__iter = aux.getRight()
            return aux.getData()
        except:
            raise StopIteration;
